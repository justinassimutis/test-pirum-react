const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const Webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function(env) {
  return {
    entry: {
      main: path.resolve(__dirname, '..', 'src', 'app', 'entryPoints', 'main'),
      vendor: ['react', 'react-dom']
    },
    output: {
      path: path.join(__dirname, '..', 'build-dev'),
      filename: '[name].bundle.js'
    },
    module: {
      rules: [
        {
          test: /\.coffee$/,
          use: 'coffee-loader',
          exclude: /node_modules/
        },
        {
          test: /\.ya?ml$/,
          use: ['json-loader', 'yaml-loader'],
          // loader: 'json-loader!yaml-loader',
          include: path.resolve(__dirname, '..', 'app', 'config')
        },
        {
          test: /\.ts$/,
          use: 'ts-loader',
          include: path.resolve(__dirname, '..', 'app', 'ts')
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          options: {
            presets: ['react', 'stage-0', ['es2015', { modules: false }]]
          }
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
          // loader: 'style-loader!css-loader'
        },
        {
          test: /\.scss$/,
          // loaders: ['style-loader', 'css-loader', 'sass-loader']
          // loader: 'style-loader!css-loader!sass-loader'
          // use: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader') WEBPACK 1
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader!sass-loader'
          })
        },
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)(\?.*$|$)/,
          use: 'url-loader?limit=100000'
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.coffee', '.ts', '.css', '.scss', '.json'],
      alias: {
        app: path.resolve(__dirname, '..', 'src', 'app')
      }
    },
    plugins: [
      new ExtractTextPlugin('[name].css'),
      new Webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'vendor.bundle.js',
        chunks: ['vendor']
      }),

      new HtmlWebpackPlugin({
        template: path.resolve(
          __dirname,
          '..',
          'src',
          'app',
          'entryPoints',
          'main',
          'index.html'
        ),
        hash: true,
        chunks: ['vendor', 'main']
      }),
      new CleanWebpackPlugin(['build-dev'], {
        root: path.resolve(__dirname, '..'),
        verbose: true
      }),
      new Webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(env),
        env: JSON.stringify(env)
      }),
      new CopyWebpackPlugin([{ from: path.resolve(__dirname, '..', 'public') }])
    ],
    devServer: {
      contentBase: path.resolve(__dirname, '..', 'build-dev'),
      inline: true,
      port: 3000
    },
    devtool: 'eval-source-map'
    // devtool: 'eval'
    // devtool: 'cheap-eval-source-map'
  };
};
