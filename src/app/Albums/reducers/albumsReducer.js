import { Types } from '../actions';
import Immutable from 'seamless-immutable';
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = Immutable({
  songs: [],
  attemptingGetsongs: false,
  songsFetchError: null
});

const songsFetchAttempt = state =>
  state.merge({
    songsFetchError: null,
    attemptingGetsongs: true
  });

const songsFetchSuccess = (state, { songs }) =>
  state.merge({
    songsFetchError: null,
    attemptingGetsongs: false,
    songs
  });

const songsFetchFail = (state, { error }) =>
  state.merge({
    songsFetchError: error,
    attemptingGetsongs: false
  });

export default createReducer(INITIAL_STATE, {
  [Types.SONGS_FETCH_ATTEMPT]: songsFetchAttempt,
  [Types.SONGS_FETCH_SUCCESS]: songsFetchSuccess,
  [Types.SONGS_FETCH_FAIL]: songsFetchFail
});
