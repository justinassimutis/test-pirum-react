import React from 'react';
import { connect } from 'react-redux';
import Album from '../components/Album';
import { getAlbumsSorted } from '../../selectors';

const AlbumsContainer = ({ albums }) => {
  const renderedAlbums = albums.map(el => {
    const { band, album, songs } = el;
    return <Album key={band + album} {...{ band, album, songs }} />;
  });

  return (
    <div className="container">
      <ul className="list">
        {renderedAlbums}
      </ul>
    </div>
  );
};

const mapStateToProps = state => ({
  albums: getAlbumsSorted(state)
});

export default connect(mapStateToProps)(AlbumsContainer);
