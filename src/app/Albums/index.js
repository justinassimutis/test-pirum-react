import AlbumsContainer from './containers/AlbumsContainer';
import Album from './components/Album';
import { Creators } from './actions';

export default {
  AlbumsContainer,
  Album,
  Creators
};
