import React, { Component } from 'react';
import classNames from 'classnames';

class Album extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  render() {
    const { band, album, songs } = this.props;
    return (
      <li className="list-item">
        <div className="album-title">
          {band}{' - '}{album}{' '}
          <span
            className="expand"
            onClick={() => this.setState({ open: !this.state.open })}
          >
            <i
              className={classNames(
                'fa',
                this.state.open ? 'fa-minus-circle' : 'fa-plus-circle'
              )}
            />
          </span>
        </div>
        <ul
          className={classNames(
            'songs',
            this.state.open ? 'visible' : 'hidden'
          )}
        >
          {songs.map(song => <li key={song} className="list-item">{song}</li>)}
        </ul>
      </li>
    );
  }
}

export default Album;
