import 'babel-polyfill';
import './styles/main';
import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';

import Routes from 'app/core/config/routes';
import configureStore from 'app/core/config/store';

const store = configureStore();

const Main = () => (
  <Provider store={store}>
    {Routes(store)}
  </Provider>
);

const mountNode = document.getElementById('mountNode');

render(<Main />, mountNode);
