import { combineReducers } from 'redux';
import { Types } from 'app/core/actions';
import { routerReducer as routing } from 'react-router-redux';
import albumsReducer from 'app/Albums/reducers';

const appReducer = combineReducers({
  routing,
  ...albumsReducer
});

// Root reducer that resets the entire state for logout
export default (state, action) => {
  if (action.type === Types.APP_LOGOUT) {
    const { routing: routingState } = state;
    state = { routing: routingState };
  }
  return appReducer(state, action);
};
