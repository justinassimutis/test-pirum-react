import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import albums from 'app/Albums';
import Main from 'app/Main/Main';

const { Creators } = albums;

const Routes = store => {
  const { dispatch } = store;

  return (
    <Router history={syncHistoryWithStore(browserHistory, store)}>
      <Route
        path="/"
        component={Main}
        onEnter={() => dispatch(Creators.songsFetchAttempt())}
      />
    </Router>
  );
};

export default Routes;
