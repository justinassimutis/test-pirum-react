import { createActions } from 'reduxsauce';

export const { Types, Creators } = createActions({
  appLogout: null,
  appStart: null
});
