import React from 'react';
import Albums from 'app/Albums';

const { AlbumsContainer } = Albums;

const Main = () => (
  <div className="container">
    <div>
      <h1>
        <i className="fa fa-music fa-lg" aria-hidden="true" />
      </h1>
      <AlbumsContainer />
    </div>
  </div>
);

export default Main;
